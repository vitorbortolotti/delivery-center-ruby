# Delivery Center Ruby Interview

This project is the result of the Ruby interview test proposed by Delivery Center. 
The test requirements can be found [here](https://bitbucket.org/delivery_center/test-dev-backend/src/master/).

## The solution

My main objective developing this solution was to create a simple yet reliable integration API, 
that is easy to maintain and gives a clear visibility of the tasks being executed. 

The solution used is inspired by the 
[Builder Design Pattern](https://sourcemaking.com/design_patterns/builder), where the class `Builders::Order` is 
responsible for building an order from the JSON payload sent by the marketplace. Once an `Order` is created and
validated, a request is made to the Delivery Center API to complete the integration. 

The stack is a Rails 6 API, with:
 
- [ActiveModelSerializers](https://rubygems.org/gems/active_model_serializers/versions/0.10.2?locale=pt-BR) for object serialization
- [Faraday](https://github.com/lostisland/faraday) for HTTP/REST requests
- [Bugsnag](https://www.bugsnag.com/) for error monitoring & reporting

### Relevant keypoints

- None of the models of the app are persisted in the database (except IntegrationLog, for logging). Trying to 
normalize and persist all data passing through the application usually adds unnecessary complexity and makes it harder to 
mantain. So, in this case, only the JSON payload is persisted in the `integration_logs` table. 
- The `IntegrationLog` model is used to log the status and payload of all requests received by the API. This can be 
really useful when debugging unhandled requests and for generating reports.

### Nice-to-have features

- Some sort of background task runner, like Resque or Sidekiq, to handle failed requests to the Delivery Center API 
with a retry strategy.
- More test cases using FactoryBot factories to generate marketplace payloads with random data.

## Getting Started

> If you don't feel like running the app locally, you can test it using the Heroku version: https://sheltered-everglades-76945.herokuapp.com

To test the app locally, make sure you have Docker and Docker Compose installed, then run 

```bash
docker-compose up
```

To setup the database, execute in another terminal

```bash
docker exec delivery-center-ruby_web_1 bundle exec rake db:setup db:migrate
```

If everything goes right, the app should be serving at http://localhost:3000.

## Usage

The API has only two endpoints for testing the integration flow. Both expect a POST with the body being 
a JSON structured as provided the test requirements.

### `POST /order_create`

Builds the processed json (if the validation passes) and makes a request to the Delivery Center API 
(https://delivery-center-recruitment-ap.herokuapp.com) with the constructed payload.

Example response:

```json
{
  "success": true,
  "api_response": {
    "status": 200,
    "body": "OK",
    "response_headers": {
      "server": "Cowboy",
      "date": "Wed, 08 Apr 2020 01:24:34 GMT",
      "connection": "keep-alive",
      "content-type": "text/html;charset=utf-8",
      "x-xss-protection": "1; mode=block",
      "x-content-type-options": "nosniff",
      "x-frame-options": "SAMEORIGIN",
      "content-length": "2",
      "via": "1.1 vegur"
    }
  }
}
```

[Here](https://gist.github.com/vitorbortolotti/2314c9d65225c9d06b4b19701a8163a8) you can find a gist with 
an example curl request

### `POST /order_parse`

Builds the processed JSON (if the validation passes) and returns the serialized object. 

> The JSON returned here is exactly the payload that will be sent to the Delivery Center API in the 
> `/order_create` endpoint.

Example response:

```json
{
  "externalCode": "9987071",
  "storeId": 123,
  "subTotal": "49.90",
  "deliveryFee": "5.14",
  "total_shipping": "5.14",
  ...
}
```

[Here](https://gist.github.com/vitorbortolotti/2811a2ed6c277b8838b6a45212a22546) you can find a gist with 
an example curl request
