class OrderItem < ActiveModelSerializers::Model
  attr_accessor :external_code, :name, :price, :quantity, :total, :sub_items

  validates :external_code, :name, :price, :quantity, :total, presence: true
  validates_numericality_of :price, :quantity, :total
end
