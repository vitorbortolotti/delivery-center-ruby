class Customer < ActiveModelSerializers::Model
  attr_accessor :external_code, :name, :email, :contact

  validates :external_code, :name, :email, :contact, presence: true
end
