class IntegrationLog < ApplicationRecord

  enum action: [:order_create]
  enum status: [:success, :error]

end
