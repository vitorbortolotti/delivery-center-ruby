class Payment < ActiveModelSerializers::Model
  attr_accessor :type, :value

  validates :type, :value, presence: true
  validates_numericality_of :value
end
