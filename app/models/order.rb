class Order < ActiveModelSerializers::Model

  attr_accessor :external_code, :store_id, :dt_order_create, :sub_total, :delivery_fee, :total,
                :customer, :items, :payments, :country, :state, :city, :district, :street, :complement,
                :latitude, :longitude, :postal_code, :number

  validates :external_code, :store_id, :dt_order_create, :sub_total, :delivery_fee, :total,
            :customer, :items, :payments, :country, :state, :city, :district, :street, :complement, :latitude,
            :longitude, :postal_code, :number, presence: true

  validate :customer_must_be_valid, :items_must_be_valid, :payments_must_be_valid
  validates_numericality_of :sub_total, :delivery_fee, :total, :latitude, :longitude

  alias_attribute :total_shipping, :delivery_fee

  private

  def customer_must_be_valid
    return unless self.customer.present?

    unless self.customer.valid?
      self.errors.add(:customer, self.customer.errors.full_messages)
    end
  end

  def items_must_be_valid
    return unless self.items.present?

    self.items.each do |item|
      unless item.valid?
        self.errors.add(:items, item.errors.full_messages)
      end
    end
  end

  def payments_must_be_valid
    return unless self.payments.present?

    self.payments.each do |payment|
      unless payment.valid?
        self.errors.add(:payment, payment.errors.full_messages)
      end
    end
  end
end
