require "#{Rails.root}/lib/brazil_states"

# This is the main class responsible for extracting data from the payload
# and building an instance of Order.
# It doesn't make validations on the payload itself, but expects the Order model
# to validate if the provided attributes are actually valid.
class Builders::Order
  include ActiveModel::Model

  attr_accessor :payload

  validates :payload, presence: true

  def initialize(payload)
    self.payload = payload
  end

  def build
    self.validate!

    @order = Order.new

    build_base
    build_address
    build_customer
    build_items
    build_payments

    @order.validate!
    @order
  end

  private

  def build_base
    @order.store_id        = payload.dig('store_id')
    @order.external_code   = payload.dig('id')&.to_s
    @order.sub_total       = transform_price(payload.dig('total_amount'))
    @order.delivery_fee    = transform_price(payload.dig('total_shipping'))
    @order.total           = transform_price(payload.dig('total_amount_with_shipping'))
    @order.dt_order_create = transform_date(payload.dig('date_created'))
  end

  def build_address
    @order.country     = payload.dig('shipping', 'receiver_address', 'country', 'id')
    @order.city        = payload.dig('shipping', 'receiver_address', 'city', 'name')
    @order.district    = payload.dig('shipping', 'receiver_address', 'neighborhood', 'name')
    @order.street      = payload.dig('shipping', 'receiver_address', 'street_name')
    @order.number      = payload.dig('shipping', 'receiver_address', 'street_number')
    @order.complement  = payload.dig('shipping', 'receiver_address', 'comment')
    @order.latitude    = payload.dig('shipping', 'receiver_address', 'latitude')
    @order.longitude   = payload.dig('shipping', 'receiver_address', 'longitude')
    @order.postal_code = payload.dig('shipping', 'receiver_address', 'zip_code')
    @order.state       = transform_state(payload.dig('shipping', 'receiver_address', 'state', 'name'))
  end

  def build_customer
    @order.customer = Customer.new(
      external_code: payload.dig('buyer', 'id')&.to_s,
      name:          payload.dig('buyer', 'nickname'),
      email:         payload.dig('buyer', 'email'),
      contact:       "#{payload.dig('buyer', 'phone', 'area_code')}#{payload.dig('buyer', 'phone', 'number')}",
    )
  end

  def build_items
    return nil unless payload['order_items'].present?

    @order.items = payload['order_items'].map do |order_item|
      OrderItem.new(
        external_code: order_item.dig('item', 'id'),
        name:          order_item.dig('item', 'title'),
        price:         order_item.dig('unit_price'),
        quantity:      order_item.dig('quantity'),
        total:         order_item.dig('full_unit_price'),
        sub_items: []
      )
    end
  end

  def build_payments
    return nil unless payload['payments'].present?

    @order.payments = payload['payments'].map do |payment|
      Payment.new(
        type: transform_payment_type(payment['payment_type']),
        value: payment['total_paid_amount']
      )
    end
  end

  def transform_date(date)
    return unless date.present?

    DateTime.iso8601(date).utc.iso8601(3)
  rescue => e
    raise "Could not transform date #{date}"
  end

  def transform_state(name)
    return unless name.present?

    if BrazilStates.find_by_name(name).present?
      BrazilStates.abbreviate(name)
    else
      raise 'Unknown state name'
    end
  end

  def transform_payment_type(payment_type)
    return unless payment_type.present?

    # Assuming there would exist multiple payment types
    case payment_type
    when 'credit_card'
      'CREDIT_CARD'
    else
      raise 'Unknown payment type'
    end
  end

  def transform_price(number)
    return unless number.present?

    sprintf('%.2f', number)
  rescue
    raise "Could not transform price #{number}"
  end

end
