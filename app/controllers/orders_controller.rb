class OrdersController < ActionController::API

  before_action :permit_all_params, :build_order

  def create
    response = DeliveryCenterService.order_create(@order)

    if response.success?
      IntegrationLog.create(action: :order_create, payload: params, status: :success)
      render json: { success: true, api_response: response }, status: :ok
    else
      IntegrationLog.create(action: :order_create, payload: params, status: :error)
      render json: { success: false, error: response.body }, status: :unprocessable_entity
    end
  end

  def parse
    render json: @order, status: :ok, vitor: 'bar'
  end

  private

  # Since Order Builder will cherry-pick values from the payload, this won't
  # create the risk of updating unexpected fields
  def permit_all_params
    params.permit!
  end

  def build_order
    @order = Builders::Order.new(params).build
  rescue => e
    Bugsnag.notify(e)
    IntegrationLog.create(action: :order_create, payload: params, status: :error)
    render json: { success: false, error: e.message }, status: :bad_request
  end

end
