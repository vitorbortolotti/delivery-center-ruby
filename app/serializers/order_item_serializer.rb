class OrderItemSerializer < ActiveModel::Serializer
  include KeyCamelizable

  attributes :external_code, :name, :price, :quantity, :total, :sub_items
end
