class CustomerSerializer < ActiveModel::Serializer
  include KeyCamelizable

  attributes :external_code, :name, :email, :contact
end
