class OrderSerializer < ActiveModel::Serializer
  include KeyCamelizable

  attributes :external_code, :store_id, :sub_total, :delivery_fee, :total_shipping, :total, :country,
             :state, :city, :district, :street, :complement, :latitude, :longitude, :dt_order_create,
             :postal_code, :number

  has_one  :customer
  has_many :items
  has_many :payments

  def skip_key_camelization
    [:total_shipping]
  end
end
