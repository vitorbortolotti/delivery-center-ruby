class PaymentSerializer < ActiveModel::Serializer
  include KeyCamelizable

  attributes :type, :value
end
