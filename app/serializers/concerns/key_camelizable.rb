module KeyCamelizable
  extend ActiveSupport::Concern

  # This is a workaround to make possible to serialize all keys in camelCase except for specific ones,
  # in this project, :total_shipping
  included do
    def attributes(*args)
      super.deep_transform_keys do |key|
        if self.methods.include?(:skip_key_camelization) && self.skip_key_camelization.include?(key)
          key.to_s
        else
          key.to_s.camelize(:lower)
        end
      end
    end
  end
end
