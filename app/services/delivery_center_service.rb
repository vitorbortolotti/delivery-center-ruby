class DeliveryCenterService

  API_URL = 'https://delivery-center-recruitment-ap.herokuapp.com/'

  def self.order_create(order)
    Faraday.post(API_URL) do |req|
      req.headers['Content-Type'] = 'application/json'
      req.headers['X-Sent'] = Time.now.strftime('%Hh%M - %d/%m/%y')
      req.body = ActiveModelSerializers::SerializableResource.new(order, adapter: :attributes).to_json
    end
  rescue => e
    Bugsnag.notify(e, error_message: 'could not create order on DeliveryCenterService')
    raise e
  end

end
