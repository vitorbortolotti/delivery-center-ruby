Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  post 'order_create', to: 'orders#create'
  post 'order_parse',  to: 'orders#parse'
end
