class CreateIntegrationLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :integration_logs do |t|
      t.integer :action
      t.json :payload
      t.integer :status
      t.timestamps
    end
  end
end
