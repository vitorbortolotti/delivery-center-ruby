require 'rails_helper'

RSpec.describe Builders::Order, type: :model do

  let!(:base_payload) { JSON.parse(File.read(File.join(Rails.root, 'spec', 'support', 'order_build_payloads', 'base.json'))) }

  describe '#build' do

    subject do
      Builders::Order.new(base_payload).build
    end

    context 'when the payload is valid' do

      it 'returns an instance of Order' do
        expect(subject).to be_a(Order)
      end

    end

    context 'when the payload is not valid' do

      before do
        base_payload['order_items'] = nil
      end

      it 'raises a validation error' do
        expect{ subject }.to raise_error(ActiveModel::ValidationError)
      end

    end

  end

  describe '#build_base' do

    subject do
      Builders::Order.new(base_payload)
    end

    context 'when the payload is valid' do

      it 'updates the store_id attribute' do
        expect(subject.build.store_id).to eq(282)
      end

      it 'updates the external_code attribute' do
        expect(subject.build.external_code).to eq('9987071')
      end

      it 'updates the sub_total attribute' do
        expect(subject.build.sub_total).to eq('49.90')
      end

      it 'updates the delivery_fee attribute' do
        expect(subject.build.delivery_fee).to eq('5.14')
      end

      it 'updates the total attribute' do
        expect(subject.build.total).to eq('55.04')
      end

      it 'updates the dt_order_create attribute' do
        expect(subject.build.dt_order_create).to eq('2019-06-24T20:45:32.000Z')
      end

    end

  end

  describe '#build_address' do

    subject do
      Builders::Order.new(base_payload)
    end

    context 'when the shipping object is not present' do

      before do
        base_payload['shipping'] = nil
      end

      it 'does not raise a validation error' do
        expect(subject.valid?).to be(true)
      end

    end

    context 'when the receiver_address object is not present' do

      before do
        base_payload['shipping']['receiver_address'] = nil
      end

      it 'does not raise a validation error' do
        expect(subject.valid?).to be(true)
      end

    end

    context 'when the payload is valid' do

      it 'updates the country attribute' do
        expect(subject.build.country).to eq('BR')
      end

      it 'updates the state attribute' do
        expect(subject.build.state).to eq('SP')
      end

      it 'updates the city attribute' do
        expect(subject.build.city).to eq('Cidade de Testes')
      end

      it 'updates the district attribute' do
        expect(subject.build.district).to eq('Vila de Testes')
      end

      it 'updates the street attribute' do
        expect(subject.build.street).to eq('Rua Fake de Testes')
      end

      it 'updates the number attribute' do
        expect(subject.build.number).to eq('3454')
      end

      it 'updates the complement attribute' do
        expect(subject.build.complement).to eq('teste')
      end

      it 'updates the latitude attribute' do
        expect(subject.build.latitude).to eq(-23.629037)
      end

      it 'updates the longitude attribute' do
        expect(subject.build.longitude).to eq(-46.712689)
      end

      it 'updates the postal_code attribute' do
        expect(subject.build.postal_code).to eq('85045020')
      end

    end

  end

  describe '#build_customer' do

    subject do
      Builders::Order.new(base_payload)
    end

    context 'when the buyer object is not present' do

      before do
        base_payload['buyer'] = nil
      end

      it 'does not raise a validation error' do
        expect(subject.valid?).to be(true)
      end

    end

    context 'when the payload is valid' do

      it 'updates the external_code attribute' do
        expect(subject.build.customer.external_code).to eq('136226073')
      end

      it 'updates the name attribute' do
        expect(subject.build.customer.name).to eq('JOHN DOE')
      end

      it 'updates the email attribute' do
        expect(subject.build.customer.email).to eq('john@doe.com')
      end

      it 'updates the contact attribute' do
        expect(subject.build.customer.contact).to eq('41999999999')
      end

    end

  end

  describe '#build_items' do

    subject do
      Builders::Order.new(base_payload)
    end

    context 'when the order_items key is not present' do

      before do
        base_payload.except!('order_items')
      end

      it 'does not raise a validation error' do
        expect(subject.valid?).to be(true)
      end

    end

    context 'when the order_items key is present but empty' do

      before do
        base_payload['order_items'] = []
      end

      it 'does not raise a validation error' do
        expect(subject.valid?).to be(true)
      end

    end

    context 'when the order_items key has items' do

      let!(:order_items_count) { rand(1..20) }

      before do
        items = []
        order_items_count.times do
          items << base_payload['order_items'].first
        end
        base_payload['order_items'] = items
      end

      it 'creates exactly the number of order_items' do
        expect(subject.build.items.length).to eq(order_items_count)
      end

    end

    context 'when the payload is valid' do

      it 'updates the external_code attribute' do
        expect(subject.build.items.first.external_code).to eq('IT4801901403')
      end

      it 'updates the name attribute' do
        expect(subject.build.items.first.name).to eq('Produto de Testes')
      end

      it 'updates the price attribute' do
        expect(subject.build.items.first.price).to eq(49.9)
      end

      it 'updates the quantity attribute' do
        expect(subject.build.items.first.quantity).to eq(1)
      end

      it 'updates the total attribute' do
        expect(subject.build.items.first.total).to eq(49.9)
      end

    end

  end

  describe '#build_payments' do

    subject do
      Builders::Order.new(base_payload)
    end

    context 'when the payments key is not present' do

      before do
        base_payload.except!('payments')
      end

      it 'does not raise a validation error' do
        expect(subject.valid?).to be(true)
      end

    end

    context 'when the payments key is present but empty' do

      before do
        base_payload['payments'] = []
      end

      it 'does not raise a validation error' do
        expect(subject.valid?).to be(true)
      end

    end

    context 'when the payments key has items' do

      let!(:payments_count) { rand(1..20) }

      before do
        items = []
        payments_count.times do
          items << base_payload['payments'].first
        end
        base_payload['payments'] = items
      end

      it 'creates exactly the number of payments' do
        expect(subject.build.payments.length).to eq(payments_count)
      end

    end

    context 'when the payload is valid' do

      it 'updates the type attribute' do
        expect(subject.build.payments.first.type).to eq('CREDIT_CARD')
      end

      it 'updates the name attribute' do
        expect(subject.build.payments.first.value).to eq(55.04)
      end

    end

  end

  describe 'transformers' do

    describe '#transform_date' do

      subject do
        Builders::Order.new(base_payload)
      end

      context 'when the date is null' do

        it 'returns nil' do
          expect(subject.send(:transform_date, nil)).to be_nil
        end

      end

      context 'when the date is invalid' do

        it 'raises an error' do
          expect{ subject.send(:transform_date, 'FFF') }.to raise_error(/Could not transform/)
        end

      end

      context 'when the date is valid' do

        it 'returns a string' do
          expect(subject.send(:transform_date, '2019-06-24T16:45:33.000-04:00')).to be_a(String)
        end

        it 'converts the date to UTC' do
          parsed_date = subject.send(:transform_date, '2019-06-24T16:45:33.000-04:00')
          expect(DateTime.parse(parsed_date).utc?).to be(true)
        end

      end

    end

    describe '#transform_state' do

      subject do
        Builders::Order.new(base_payload)
      end

      context 'when the state is null' do

        it 'returns nil' do
          expect(subject.send(:transform_date, nil)).to be_nil
        end

      end

      context 'when the state does not exist' do

        it 'raises an error' do
          expect{ subject.send(:transform_state, 'Lorem') }.to raise_error('Unknown state name')
        end

      end

      context 'when the state exists' do

        it 'returns the state abbreviation' do
          expect(subject.send(:transform_state, 'Espírito Santo')).to eq('ES')
        end

      end

    end

    describe '#transform_payment_type' do

      subject do
        Builders::Order.new(base_payload)
      end

      context 'when the payment type is null' do

        it 'returns nil' do
          expect(subject.send(:transform_date, nil)).to be_nil
        end

      end

      context 'when the payment type does not exist' do

        it 'raises an error' do
          expect{ subject.send(:transform_payment_type, 'Lorem') }.to raise_error('Unknown payment type')
        end

      end

      context 'when the payment type exists' do

        it 'returns a corresponding payment type' do
          expect(subject.send(:transform_payment_type, 'credit_card')).not_to be_nil
        end

      end

    end

    describe '#transform_price' do

      subject do
        Builders::Order.new(base_payload)
      end

      context 'when the price is null' do

        it 'returns nil' do
          expect(subject.send(:transform_date, nil)).to be_nil
        end

      end

      context 'when the price is invalid' do

        it 'raises an error' do
          expect{ subject.send(:transform_price, 'FFF') }.to raise_error(/Could not transform/)
        end

      end

      context 'when the price is valid' do

        it 'returns a string' do
          expect(subject.send(:transform_price, 14.0)).to be_a(String)
        end

        it 'formats the number with two decimal cases' do
          expect(subject.send(:transform_price, 14.0)).to match(/^\d*\.\d{2}$/)
        end

      end

    end

  end

end
