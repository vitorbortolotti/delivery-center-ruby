require 'rails_helper'

RSpec.describe OrderItem, type: :model do

  it { should validate_presence_of(:external_code) }
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:price) }
  it { should validate_presence_of(:quantity) }
  it { should validate_presence_of(:total) }

  it { should validate_numericality_of(:price) }
  it { should validate_numericality_of(:quantity) }
  it { should validate_numericality_of(:total) }

end
