require 'rails_helper'

RSpec.describe Order, type: :model do

  it { should validate_presence_of(:external_code) }
  it { should validate_presence_of(:store_id) }
  it { should validate_presence_of(:dt_order_create) }
  it { should validate_presence_of(:sub_total) }
  it { should validate_presence_of(:delivery_fee) }
  it { should validate_presence_of(:total) }
  it { should validate_presence_of(:customer) }
  it { should validate_presence_of(:items) }
  it { should validate_presence_of(:payments) }

  describe 'serialization' do

    context 'when created from a valid payload' do

      let!(:base_payload)     { JSON.parse(File.read(File.join(Rails.root, 'spec', 'support', 'order_build_payloads', 'base.json'))) }
      let!(:expected_output)  { JSON.parse(File.read(File.join(Rails.root, 'spec', 'support', 'order_build_payloads', 'processed.json'))) }
      let!(:order)            { Builders::Order.new(base_payload).build }

      subject do
        ActiveModelSerializers::SerializableResource.new(order, adapter: :attributes).as_json
      end

      it 'returns the expected outputs json' do
        diff = subject.with_indifferent_access.to_a - expected_output.with_indifferent_access.to_a
        expect(diff).to be_empty
      end

    end

  end

end
