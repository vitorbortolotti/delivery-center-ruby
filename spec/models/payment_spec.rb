require 'rails_helper'

RSpec.describe Payment, type: :model do

  it { should validate_presence_of(:type) }
  it { should validate_presence_of(:value) }

  it { should validate_numericality_of(:value) }

end
